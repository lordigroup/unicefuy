﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unicefuy.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            if (Session["LogedUserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Index2()
        {
            if (Session["LogedUserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Unicefuy.Models.Usuario u)
        {
            if (ModelState.IsValid) 
            {
                using (Unicefuy.Models.unicefuyEntities dc = new Unicefuy.Models.unicefuyEntities())
                {
                    
                    var v = dc.Usuario.Where(a => a.UsuarioNombre.Equals(u.UsuarioNombre)).FirstOrDefault();
                    if (v != null)
                    {
                        //Si a futuro se guarda la clave encriptada aca deberiamos desencriptarla
                        string claveDesencriptada = v.UsuarioClave;

                        if (u.UsuarioClave != claveDesencriptada)
                        {
                            ViewBag.Message = "La clave ingresada es incorrecta. Verifique su clave y reintente por favor.";
                            return View(u);
                        }

                        Session["LogedUserID"] = v.UsuarioID.ToString();
                        Session["LogedUserFullname"] = v.UsuarioNombreCompleto;
                        Session["PerfilId"] = v.PerfilID;
                        return RedirectToAction("index2");
                    }
                    else {
                        ViewBag.Message = "El usuario ingresado es incorrecto. Verifique su usuario y reintente por favor.";
                    }
                }
            }
            return View(u);
        }

        public ActionResult Logoff()
        {
            Session["LogedUserID"] = null;
            Session["LogedUserFullname"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}