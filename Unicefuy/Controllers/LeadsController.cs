﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unicefuy.Controllers
{
    public class LeadsController : Controller
    {
        // GET: Leads
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search()
        {
            if (Session["LogedUserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login","Home");
            }
            
        }

        public ActionResult Import()
        {
            if (Session["LogedUserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
    }
}